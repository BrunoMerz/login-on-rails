class JsonWebToken
    
    def self.encode(payload)
        secret = ENV["DEVISE_JWT_SECRET_KEY"]
        JWT.encode(payload, secret)
    end

    def self.decode(token)
        secret = ENV["DEVISE_JWT_SECRET_KEY"]
        begin
            decoded = JWT.decode(token, secret)
        rescue => exception
            return nil
        end
    end
end