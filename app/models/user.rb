class User < ApplicationRecord
    has_secure_password
    has_many :posts
    validates :email, uniqueness: true
    
    enum kind: {
        admin: 0,
        standard: 1
    }

end
