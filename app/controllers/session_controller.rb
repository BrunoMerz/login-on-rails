class SessionController < ApplicationController
  def login
    @user = User.find_by(email: params[:user][:email])
    @user = @user&.authenticate(params[:user][:password])

    if @user
      token = JsonWebToken.encode(used_id: @user.id)
      render json: {token: token,user: @user}
    else
      render json: {error: "Unauthorized"}
    end
  end

  protected
    def must_be_sigining_in
        if current_user.nil?
            render json: {message: "Permissão negada"} 
        end
    end

end
