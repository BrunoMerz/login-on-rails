class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource

 
  def index
    @users = User.all

    render json: @users
  end


  def show
    render json: @user
  end


  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @user.destroy
  end

  private
    def set_user
      @user = User.find(params[:id])
    end
  
  private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmatio)
    end
end
