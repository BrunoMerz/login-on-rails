Rails.application.routes.draw do
  resources :posts
  resources :users, expect: [:create]
  get '/post', to: 'post#index'
  post '/post', to: 'post#create'
  post '/sign_up', to: 'register#sign_up'
  post '/login', to: 'session#login'
  get '/profile', to: 'application#user_must_exist'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
